package controllers.consultas

import javax.inject._
import play.api._
import play.api.mvc._

@Singleton
class ConsultasPolla @Inject() extends Controller {

  def obtenerPollaPorId(id: Int) = Action {
    if (id == 1) {
      Ok("Polla 1")
    } else {
      Ok("Otra polla: " + id)
    }
  }
}
